## Requirements
NodeJS
NPM latest
See here: https://docs.npmjs.com/getting-started/installing-node

## To start
npm start
This will run a server on http://127.0.0.1:8000/
Opening that URL will render a basic template of our site for you to copy in and modify HTML content

## Styling content
index.html contains the default template for our site. Find and replace the area "<!-- content here -->" with the page to adapt

## HTML to modify
Refer to /htmlcontent/*.html. Each file represents a URL for you to ensure works in a responsive manner. Copy the HTML into the index.html and modify.

## Style guide
We have a style guide located here for reference: https://www.appliancesonline.com.au/responsiveStyleGuide.aspx

## CSS
In the content you will recieve, there will be a lot of styling. Either directly on the element, or a <style /> tag.
We need you to get rid of it. Any stylings you do, should adhere to the style guide (see below). However, if you do require any custom CSS, please 
add it to a centralised CSS file eg '/css/topic-pages.css'

Any existing CSS files (eg '/images/opt/{{something}}.css') can remain. Its inline styles which are a problem

### No use of !important is allowed

## XML Packages
In the content you recieve, you will likely come across (!XmlPackage name="{{name}}" {{variableName}}={{variableValue}} !)content. There will be different types. Refer to the table below to see how to handle each


| XMLPackage Name | What to do | Notes |
|-----------------|------------|-------|
| (!XmlPackage name="testimonials-and-reviews.xml.config" CategoryName="{{variableValue}}") | Open the main link and take the testominal html content  | This content has not been updated since 2015, so the content is not dynamic.        |
| (!XmlPackage Name="promotion.menu" cached="true"!)  | Don't convert any pages with this. The content is dynamically generated and needs to be updated there  | -        |
| (!XmlPackage name="entity.url.xml.config" typeid="3" cached="true" entityid="charity-childrens-foundation-australia"!) | This is a generated URL. Open the original URL to obtain the URL its linking to  | -        |
| (!XmlPackage name="entity.bestbuys.razor.xml.config" bestbuyid="96238716-853c-48b9-b258-9e6be94dea43"  !) |  This generates 'best buys' HTML. This page will require a rewrite on our part if its needed | - |



XML packages are legacy content for our site. Removing all references and replacing any relevant content is very important for us moving forward

## Javascript
Similar to CSS content, any inline Javascript should be moved to a centralised js file eg '/js/topic-pages.js' which will allow us to manage code more easily

## IFrames
We don't want them anymore. Any cases of the src being a youtube video, see the "Youtube embeds" section below to replace them. If they are not youtube, open the content and add it into the current html

## Youtube embeds
Replace any youtube embedding with the following:

...
<aol-video video-id='Do9L1pcGuG8' video-type='youtube' is-responsive='true'></aol-video>
...

Where the video id is the id of the youtubevideo. In the following example the id is jI-kpVh6e1U
> https://www.youtube.com/watch?v=jI-kpVh6e1U 
